# ***penguinRice***

i dont use github anymore, pls switch to codeberg

- 👩‍💻 ***Author***: [p3nguin-kun](https://codeberg.org/p3nguin-kun)
- ***Wiki***: https://codeberg.org/p3nguin-kun/penguinRice/wiki
- 🗨️ ***Discord server***: [penguin clan](https://discord.gg/https://discord.gg/yzn442FGuZ)

***Status***: Complete

**[Ubuntu](https://ubuntu.com) is bloated! [Arch Linux](https://archlinux.org) with [penguinRice](https://codeberg.org/p3nguin-kun/penguinRice) is better! <(￣︶￣)>**

**This script will automatically install fully-featured tiling/floating window manager-based system on any Arch Linux or Arch-based distro.**

Showcase video:

[<img src="https://img.youtube.com/vi/aYcmIjYeFaU/maxresdefault.jpg" width="600" height="300"/>](https://www.youtube.com/embed/aYcmIjYeFaU)

# ***Menu***
- [Screenshots](#screenshots)
- [Packages](#packages)
- [Types of penguinRice users](#types-of-penguinrice-users)
- [What does this script do?](#what-does-this-script-do)
- [Style selector](#style-selector)
- [Installation](#installation)
- [Tutorial](#tutorial)
- [Support](#support)

# ***Screenshots***

| Everforest | Tokyo Night | Gruvbox |
| :--------: | :---------: | :-----: |
| ![img](https://i.imgur.com/B9SdLpk.png) | ![img](https://i.imgur.com/wMMFW8w.png) | ![img](https://i.imgur.com/E2vApoC.png) |
| Nord | Catppuccin | Dracula |
| ![img](https://i.imgur.com/ABglZDS.png) | ![img](https://i.imgur.com/4AYL12A.png) | ![img](https://i.imgur.com/ZbMtf6x.png) |
| Monochrome |
| ![img](https://i.imgur.com/Keg5lHS.png) |

# ***Packages***
- 🔴 ***Dotfiles***: [penguinDotfiles](https://codeberg.org/p3nguin-kun/penguinDotfiles)
- 🪟 ***Window Manager***: [i3](https://i3wm.org), [bspwm](https://github.com/baskerville/bspwm) and [Openbox](http://openbox.org/wiki/Main_Page)
- 📊 ***Status bar***: [polybar](https://github.com/polybar/polybar), [plank](https://launchpad.net/plank) (openbox)
- 👨‍💻 ***Terminal***: [alacritty](https://alacritty.org/)
- 🌐 ***Browser***: [Brave](https://brave.com/)
- 🗂️ ***File Manager***: [ranger](https://ranger.github.io/) and [Thunar](https://docs.xfce.org/xfce/thunar/start) (Openbox only)
- 🐚 ***Shell***: [fish](https://fishshell.com/)
- 🏘️ ***Launcher***: [Rofi](https://github.com/davatorium/rofi)
- 📄 ***Text editor***: [NeoVim](https://neovim.io) with [NvChad](https://github.com/NvChad/NvChad)
- 📄 ***PDF Viewer***: [zathura](https://pwmt.org/projects/zathura/)
- 📅 ***Calendar***: [calcurse](https://calcurse.org/)
- 🎞️ ***Media player***: [mpv](https://mpv.io) and [ncmpcpp](https://github.com/ncmpcpp/ncmpcpp)
- 🖼️ ***Photo viewer***: [feh](https://feh.finalrewind.org/)
- ℹ️ ***Fetch***: [lmaofetch](https://codeberg.org/p3nguin-kun/lmaofetch)
- 📊 ***System monitor***: [btop](https://github.com/aristocratos/btop)
- 🖥️ ***Manage screens***: [ARandR](https://christian.amsuess.com/tools/arandr/)
- 🔔 ***Notification***: [dunst](https://dunst-project.org/)
- 🖵 ***Compositor***: [picom](https://github.com/yshui/picom)
- 🔒 ***Lockscreen***: [betterlockscreen](https://github.com/betterlockscreen/betterlockscreen)

# ***Types of penguinRice users***
- People who already know their stuff and just want to automate installing a system without doing the boring stuff you’ve done a million times.
- Those who want to use hacker computer setup like in many movies.
- People who want to automatically rice their Linux desktop.
- Who want productivity Linux desktop for study and work.
- Those who spend a whole day to install Arch Linux manually but lazy  to rice, config

# ***What does this script do?***
- Install some necessary packages such as Firefox, LibreOffice, NeoVim, ...
- Automatically rice tiling window manager
- Automatically config software such as neovim, neofetch, dunst, pacman (for what?), ...
- Customize your computer
- Automatically install yay, an AUR helper
- Config Firefox with FirefoxCSS, harden with arkenfox user.js and install uBlock Origin
- Change ```Caps Lock``` to ```Escape``` (because nobody use Caps Lock)

# ***Style selector***
penguinRice now has style selector for people want to change desktop's style, just press ```Alt + space``` and style selector will appear on your screen

![img](https://i.imgur.com/KaAwop3.png)

# ***Installation***

***Note: You need to login as normal user to use this script***

1. Install [Arch Linux](https://archlinux.org) or any Arch-based distro
> You can use [archinstall](https://wiki.archlinux.org/title/archinstall) to install [Arch Linux](https://archlinux.org) (Choose Minimal profile)

2. Install [Git](https://git-scm.com/) if you don't have it
```
sudo pacman -S git
```

3. Clone this repository
```
git clone https://codeberg.org/p3nguin-kun/penguinRice
```

4. Go to penguinRice directory
```
cd penguinRice
```

5. Run this script
```
sh penguinrice.sh
```

6. Follow the instruction

7. Restart your computer

8. Done! Now you can use fully-featured floating/tiling window manager on your computer

# ***Tutorial***
- penguinRice has a keybinding list, you can read it by pressing ``` Super + ` ``` or click [here](hhttps://codeberg.org/p3nguin-kun/penguinRice/wiki/2.-Keybindings-and-commands)
- You can read penguinRice's wiki [here](https://codeberg.org/p3nguin-kun/penguinRice/wiki)

# ***Contributions***

1. Fork this project.
2. Edit the scripts, add/edit the keybindings.
3. Make a pull request.